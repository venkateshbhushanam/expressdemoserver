const express = require('express');
const app = express();
const port = process.env.PORT || 4000;

app.get('/', (req, res) => {
    res.status(200);
    res.send("My first express server");
});

app.listen(port, (error) => {
    if (!error) {
        console.log(`Server is Running and App is listening to port ${port}`)
    } else {
        console.log("Error occured, Server Not Started")
    }

})
